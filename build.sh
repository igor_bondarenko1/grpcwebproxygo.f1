#!/bin/bash

myuser=$(whoami)

echo userSSS is $myuser

rm -rf artifacts
mkdir artifacts

cd ./go/grpcwebproxy

go get .

declare -a os_arch_list=(
  "windows_386" "windows_amd64"
  "linux_amd64" "linux_arm" "linux_arm64"
  )  

  
for os_archvar in "${os_arch_list[@]}"; do
  IFS='_' read -ra items <<< "$os_archvar"
  os=${items[0]}
  arch=${items[1]}
  
  echo os and arch are $os $arch 
  
  name=grpcwebproxy
  
  if [ "$os" == "windows" ];  then 
      name=$name.exe
  fi
  
  echo  name is $name
 
  dir=artifacts/${os}_${arch}
  mkdir ../../${dir}
  
  GOOS=$os GOARCH=$arch go build  -o ../../${dir}/${name}
    
done

chmod -R 777 ../../artifacts
