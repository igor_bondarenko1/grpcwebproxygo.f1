echo "Current gopath is:" $GOPATH
echo "Current path is:" $PATH
set -e
set -x

# cd go/src/bitbucket.org/Axxonsoft/supervisor2

if [ -e GNUmakefile ]
then
    make docker
    docker run --rm -i -v $PWD:/go/src/github.com/improbable-eng/grpc-web   grpcwebproxybuild
    
else
    ./build.sh 
fi
